# ObjectID: 1000044
# Character field ID when accessed: 100000000
# ParentID: 9400025
# Object Position X: 5351
# Object Position Y: 14
# Lil Murgoth | Cloverologist
# gives level x

target_level = 30

if sm.sendAskYesNo("Would you like to level up to level " + str(target_level)):
    if sm.getChr().getLevel() < target_level:
        sm.addLevel(target_level - sm.getChr().getLevel())        
    else:
        sm.sendSayOkay("You exceed the level cap!")        
        
    sm.dispose()