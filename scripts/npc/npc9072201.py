# ParentID: 9072201
# ObjectID: 1000004
# Character field ID when accessed: 100000000
# Object Position Y: 327
# Object Position X: 1158
# NPC Jimmy-Jack | Coin Shop

def skip_bug():
    MAPLE_ADMINISTARTOR = 2007

    

    map_to_warp = 100000001  # End of
    target_level = 100

    sm.setSpeakerID(MAPLE_ADMINISTARTOR)
    sm.removeEscapeButton()
    sm.lockInGameUI(False)

    if sm.sendAskYesNo("Would you like to instantly arrive at #m" + str(map_to_warp) + "#?"):
        if sm.getChr().getLevel() < target_level:
            sm.addLevel(target_level - sm.getChr().getLevel())


        sm.warp(map_to_warp)

        sm.lockInGameUI(False)
        sm.dispose()