# NPC ID: 9000178
# Murgoth Mysterious Magician | Soul Researcher
# Location Henesys
# Multiple Boss Lobby Maps (Balrog, Pink Bean, etc)
# gives job and level

BLASTER2 = 1411
Murgoth = 9000178
HAND_BUSTER = 1472268
RUDIMENTARY_CHARGES = 2070026

sm.setSpeakerID(Murgoth)
if not sm.canHold(HAND_BUSTER):
    sm.sendSayOkay("Please make room in your equip inventory.")
    sm.dispose()


if sm.sendAskYesNo("Would you like to try the Job Advance NPC?"):
    sm.jobAdvance(BLASTER2) 
    sm.giveItem(HAND_BUSTER)
    sm.addSP(110)
    sm.giveAndEquip(RUDIMENTARY_CHARGES)   		          